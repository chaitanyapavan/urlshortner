# UrlShortener
A Spring based UrlShortener. Takes a Url in json format and returns its shortened form. The shortened form can be used to redirect to the original url.
