package UrlShortner1.app1.controller;

import org.bson.Document;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import java.util.Iterator;
import java.util.List;

@RestController
public class UrlController {
	MongoClient mongo1 = new MongoClient("localhost", 27017);
	String databaseName = "urlDatabase";
	String collectionName = "urlCollection";
	MongoDatabase database = mongo1.getDatabase(databaseName);
	int database_exists = 0;
	
	@RequestMapping(value = "/shortener", method= {RequestMethod.POST, RequestMethod.GET}, consumes = {"application/json"})
	public String shortenUrl(@RequestBody @Valid final ShortenRequest shortenRequest) {

		String longUrl = shortenRequest.getUrl();  // URL that needs to be shortened
		MongoCollection<Document> collection = database.getCollection(collectionName);
		
		BasicDBObject whereQuery = new BasicDBObject();
	    whereQuery.put("url", longUrl); // Check if url already exists
	    Iterator<Document> cursor = collection.find(whereQuery).iterator();
	    String id;
		if(cursor.hasNext()) { // if url exists 
			id = cursor.next().get("id").toString();
		}
		else { 
			if(database_exists == 0) {
				MongoCursor<String> dbsCursor = mongo1.listDatabaseNames().iterator();
			    while(dbsCursor.hasNext()) {
			        if(dbsCursor.next().equals(databaseName)) {
			        	database_exists = 1;
			        	break;
			        }
			    }
			}
			if(database_exists == 1) { 
				Document stats = database.runCommand(new Document("collStats", collectionName));
				int len1 = stats.getInteger("count");
				id =  Integer.toString(len1+1);
			}
			else{
				id = Integer.toString(1);
				database_exists = 1;
			}
			Document document = new Document("url", longUrl).append("id", id); //inserting into database
			collection.insertOne(document);
		}
		String ans = "http://localhost:8080/" + id;
		return ans;
	}
	
	@RequestMapping(value = "/{id}", method=RequestMethod.GET)
    public RedirectView redirectUrl(@PathVariable String id, HttpServletRequest request, HttpServletResponse response){

		MongoCollection<Document> collection = database.getCollection(collectionName);
		
		// Finding the url for the given id.
		BasicDBObject whereQuery = new BasicDBObject();
	    whereQuery.put("id", id);
	    MongoCursor<Document> cursor = collection.find(whereQuery).iterator();
	    if(cursor.hasNext()) {
			String url = cursor.next().getString("url");
	        RedirectView redirectView = new RedirectView();
	        redirectView.setUrl("http://" + url);
	        return redirectView;
	    }
	    else {
	    	RedirectView redirectView = new RedirectView();
	        redirectView.setUrl("http://localhost:8080/myerror");
	        return redirectView;
	    }
    }
	
	@RequestMapping(value = "/myerror", method=RequestMethod.GET)
	public String myerror() {
		return "Given Shortened Url is not present";
	}

}

//This class is created to take the incoming json object for the above original url request map.
class ShortenRequest{
 private String url;

 @JsonCreator
 public ShortenRequest() {
 }

 @JsonCreator
 public ShortenRequest(@JsonProperty("url") String url) {
     this.url = url;
 }

 public String getUrl() {
     return url;
 }

 public void setUrl(String url) {
     this.url = url;
 }
}
